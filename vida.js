function mapaBarrio() {
    let mapa = L.map('divMapa', { center: [4.6023482494106664,-74.08493041992186], zoom:15 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });mosaico.addTo(mapa);


    var polygon = L.polygon([
        [4.600369811909725,  -74.08929705619812],
        [4.607449384510583,  -74.08450126647949],
        [4.6036529457266315, -74.08008635044098],
        [4.597268466387397,  -74.08476412296295],
        [4.600368140929791,  -74.08929605036973],
    ]).addTo(mapa);
        
        var marker4 = L.marker([4.60146062677946,-74.0837100148201]).addTo(mapa);
        marker4.bindTooltip("El Bronx Distrito creativo <br> Cra. 15 Bis #9a-2 a, Bogotá").openTooltip().addTo(mapa);
        
        var marker5 = L.marker([4.604668896119288,-74.08549100160599]).addTo(mapa);
        marker5.bindTooltip("Parque Plaza España <br> Cl. 10 #75, Bogotá").openTooltip();

        var marker6 = L.marker([4.60071737565257,-74.08742755651473]).addTo(mapa);
        marker6.bindTooltip("Parque María Eugenia <br> Cl. 7 #50, Bogotá" ).openTooltip();
        var marker6 = L.marker([4.599877875245256,-74.08396482467651]).addTo(mapa);
        marker6.bindTooltip("Avena Don Manuel <br> Cl. 8 #39, Bogotá").openTooltip();



}