function mapaSitios() {
    let mapa = L.map('divMapa', { center: [4.6023482494106664,-74.08493041992186], zoom:15 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });mosaico.addTo(mapa);


    var polygon = L.polygon([
        [4.600369811909725,  -74.08929705619812],
        [4.607449384510583,  -74.08450126647949],
        [4.6036529457266315, -74.08008635044098],
        [4.597268466387397,  -74.08476412296295],
        [4.600368140929791,  -74.08929605036973],
    ]).addTo(mapa);
        
        var marker1 = L.marker([4.6035099104890564,-74.08619306981564]).addTo(mapa);
        marker1.bindTooltip("Hospital San Jose").openTooltip().addTo(mapa);
        
        var marker2 = L.marker([4.598141389574372,-74.08451065421104]).addTo(mapa);
        marker2.bindTooltip("Metropolitana de Bogotá ").openTooltip();
       
        var marker3 = L.marker([4.60182556814585,-74.08168628811835]).addTo(mapa);
        marker3.bindTooltip("Obelísco los Mártires").openTooltip();
        
        var marker3 = L.marker([4.605464277322011,-74.08583298325539]).addTo(mapa);
        marker3.bindTooltip("<br> Colegio Liceo nacional Agustin Nieto Caballero </br> Dirección: Cra. 19 #1117, Bogotá").openTooltip();


}
function agustinMapa(){


    let mapa = L.map('divMapa', { center: [4.605464277322011,-74.08583298325539], zoom:15 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });mosaico.addTo(mapa);


   

    var marker3 = L.marker([4.605464277322011,-74.08583298325539]).addTo(mapa);
        marker3.bindTooltip("<br> Colegio Liceo nacional Agustin Nieto Caballero </br> Dirección: Cra. 19 #11-17, Bogotá").openTooltip();

}
function hospitalMapa(){


    let mapa = L.map('divMapa2', { center: [4.603511915656143,-74.08619374036789], zoom:15 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });mosaico.addTo(mapa);


   

    var marker3 = L.marker([4.603511915656143,-74.08619374036789]).addTo(mapa);
        marker3.bindTooltip("<br> Hospital San José </br> Dirección: Cra. 19 #11-17, Bogotá").openTooltip();

}
function basilicaMapa(){


    let mapa = L.map('divMapa3', { center: [4.602332876437761,-74.08240981400013], zoom:15 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });mosaico.addTo(mapa);


   

    var marker3 = L.marker([4.602332876437761,-74.08240981400013]).addTo(mapa);
        marker3.bindTooltip("<br> Basílica menor del Voto Nacional </br> Dirección: Cra. 19 #11-17, Bogotá").openTooltip();

}
